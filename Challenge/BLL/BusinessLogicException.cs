﻿using System;

namespace Challenge.BLL
{
    public abstract class BusinessLogicException : Exception
    {
        public BusinessLogicException(string message = "") : base(message) { }

        public abstract void LogError();
    }
}
