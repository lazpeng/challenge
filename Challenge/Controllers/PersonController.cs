﻿using Challenge.DAL;
using Challenge.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PeopleDbContext _context;

        public PersonController(UserManager<IdentityUser> userManager, PeopleDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        private async Task<string> GetSessionUserId()
        {
            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;

            var user = await _userManager.FindByEmailAsync(email);
            return user.Id;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(new BLL.People(await GetSessionUserId(), _context).ListAll());
        }

        [Authorize]
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                return Ok(new BLL.People(await GetSessionUserId(), _context).Get(id));
            } catch (BLL.BusinessLogicException ex)
            {
                if (ex is BLL.NotFoundException)
                {
                    return NotFound();
                }
                else throw;
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Person model)
        {
            try
            {
                var result = new BLL.People(await GetSessionUserId(), _context).Create(model);
                return Created($"{result.Id}", result);
            } catch (BLL.BusinessLogicException ex)
            {
                if (ex is BLL.InvalidContentException)
                {
                    return BadRequest(new { invalidField = ex.Message });
                }
                else throw;
            }
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Person value)
        {
            try
            {
                return Ok(new BLL.People(await GetSessionUserId(), _context).Update(id, value));
            } catch (BLL.BusinessLogicException ex)
            {
                if (ex is BLL.NotAuthorizedException)
                {
                    return Unauthorized();
                }
                else if (ex is BLL.InvalidContentException)
                {
                    return BadRequest(new { invalidField = ex.Message });
                }
                else throw;
            }
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                new BLL.People(await GetSessionUserId(), _context).Delete(id);
                return Ok();
            } catch (BLL.BusinessLogicException ex)
            {
                if (ex is BLL.NotAuthorizedException)
                {
                    return Unauthorized();
                }
                else throw;
            }
        }
    }
}
