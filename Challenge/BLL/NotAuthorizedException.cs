﻿namespace Challenge.BLL
{
    public class NotAuthorizedException : BusinessLogicException
    {
        public NotAuthorizedException(string message = "") : base(message) { }

        public override void LogError()
        {
            // TODO: Log error when access to a certain feature was not authorized
        }
    }
}
