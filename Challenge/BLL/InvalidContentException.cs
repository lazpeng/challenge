﻿namespace Challenge.BLL
{
    public class InvalidContentException : BusinessLogicException
    {
        public InvalidContentException(string message = "") : base(message) { }

        public override void LogError()
        {
            // TODO: Log error for failed model validation
        }
    }
}
