﻿namespace Challenge.BLL
{
    public class NotFoundException : BusinessLogicException
    {
        public NotFoundException(string message = "") : base(message) { }

        public override void LogError()
        {
            // TODO: Log error when a value was not found
        }
    }
}
