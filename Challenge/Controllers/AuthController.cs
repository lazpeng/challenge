﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Challenge.Models;

namespace Challenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AuthController(SignInManager<IdentityUser> signInManager, IConfiguration configuration)
        {
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login", Name = "Login")]
        public async Task<IActionResult> Login([FromBody] Auth auth)
        {
            var res = await _signInManager.PasswordSignInAsync(auth.Email, auth.Password, false, false);

            if (res.Succeeded)
            {
                var authBLL = new BLL.Auth();
                return Ok(authBLL.GenerateToken(auth.Email, _configuration["JwtKey"]));
            }

            return Unauthorized();
        }

        [HttpPost]
        [Route("register", Name = "Register")]
        public async Task<IActionResult> Register([FromBody] Auth auth)
        {
            var user = new IdentityUser() { Email = auth.Email, UserName = auth.Email };
            var result = await _signInManager.UserManager.CreateAsync(user, auth.Password);

            if (result.Succeeded)
            {
                var authBLL = new BLL.Auth();
                return Ok(authBLL.GenerateToken(auth.Email, _configuration["JwtKey"]));
            }

            return Forbid();
        }
    }
}
