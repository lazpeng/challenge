﻿using System;
using System.Linq;
using Challenge.DAL;
using Challenge.Models;

namespace Challenge.BLL
{
    public class People
    {
        private readonly string _userId;
        private readonly PeopleDbContext _dbContext;

        public People(string userId, PeopleDbContext dbContext)
        {
            _userId = userId;
            _dbContext = dbContext;
        }

        public void Delete(int id)
        {
            var toDelete = _dbContext.People.Find(id);
            if (toDelete == null)
            {
                throw new NotFoundException();
            }
            else if (toDelete.OwnerId != _userId)
            {
                throw new NotAuthorizedException();
            }

            _dbContext.People.Remove(toDelete);
            _dbContext.SaveChanges();
        }

        public Person Get(int id)
        {
            var person = _dbContext.People.Find(id);

            if (person != null)
            {
                return person;
            }
            throw new NotFoundException();
        }

        public Person[] ListAll()
        {
            return _dbContext.People.ToArray();
        }

        public Person Create(Person person)
        {
            if(!person.IsValid())
            {
                throw new InvalidContentException("Cadastro falhou na validação para ser criado");
            }

            person.OwnerId = _userId;
            var added = _dbContext.People.Add(person);
            _dbContext.SaveChanges();
            person.Id = added.Entity.Id;

            return person;
        }

        public Person Update(int id, Person model)
        {
            if (!model.IsValid())
            {
                throw new InvalidContentException("Cadastro falhou na validação para ser atualizado");
            }

            var person = _dbContext.People.Find(id);
            if (person == null)
            {
                throw new NotFoundException();
            }
            else if (person.OwnerId != _userId)
            {
                throw new NotAuthorizedException();
            }

            model.Id = id;

            _dbContext.Entry(person).CurrentValues.SetValues(model);
            _dbContext.SaveChanges();
            return model;
        }
    }
}
