﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Challenge.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        public string OwnerId { get; set; }
        [StringLength(256, MinimumLength = 1)]
        [Required]
        public string Name { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [StringLength(2, MinimumLength = 2)]
        [Required]
        public string UF { get; set; }
        [RegularExpression(@"\d{3}\.\d{3}\.\d{3}\-\d{2}")]
        [Required]
        public string Cpf { get; set; }
        public DateTime DateOfBirth { get; set; }

        public bool IsUFValid()
        {
            var validUFs = new string[]
            {
                "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO",
                "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI",
                "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO",
            };

            return validUFs.Contains(UF);
        }

        public bool IsValid()
        {
            return IsUFValid() && DateTime.Compare(DateTime.Now, DateOfBirth) < 0;
        }
    }
}
