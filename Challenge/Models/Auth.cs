﻿using System.ComponentModel.DataAnnotations;

namespace Challenge.Models
{
    public class Auth
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [StringLength(256, MinimumLength = 6)]
        [Required]
        public string Password { get; set; }
    }
}
